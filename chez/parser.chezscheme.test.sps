#!/usr/bin/env chez --script

(import (parser)
	(polynomial)
	(variables))

(define p parse)

(set-variables! "xyz")

(assert (p= (p "x") '((#(1 0 0) . 1))))
(assert (p= (p "2") '((#(0 0 0) . 2))))
(assert (p= (p "x+y") '((#(1 0 0) . 1) (#(0 1 0) . 1))))
(assert (p= (p "x^2+y") '((#(2 0 0) . 1) (#(0 1 0) . 1))))
(assert (p= (p "x*z") '((#(1 0 1) . 1))))
(assert (p= (p "x^2*z") '((#(2 0 1) . 1))))
(assert (p= (p "x+3*z") '((#(1 0 0) . 1) (#(0 0 1) . 3))))
