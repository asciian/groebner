#!/usr/bin/env chez --script

(import (parser)
	(printer)
	(division)
	(groebner))

(define p (parse "x*z-y^2"))
(define q (parse "x^3-z^2"))
(define f (groebner p q))
(define g (groebner+opt p q))
(assert (for-all null? (map (lambda (s) (apply remainder s g)) f)))
(assert (for-all null? (map (lambda (s) (apply remainder s f)) g)))
