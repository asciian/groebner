#!r6rs
(library (division)
  (export divide quotients remainder)
  (import (rnrs)
	  (polynomial)
	  (term)
	  (ordering))
  
  (define (make-list n e)
    (do ([i 0 (+ i 1)]
	 [c '() (cons e c)])
	((= i n) c)))
  
  (define (divides s t)
    (for-all values (vector->list (vector-map <= (car s) (car t)))))
  
  (define (divide f . g)
    (let loop1 ([dividend f]
		[divisors g]
		[quotients (make-list (length g) '())]
		[remainder '()])
      (if (null? dividend)
	  (values quotients remainder)
	  (let loop2 ([newquotients '()]
		      [oldquotients quotients]
		      [dividend dividend]
		      [newdivisors '()]
		      [olddivisors divisors]
		      [divisionoccured #f])
	    (let ([divisor (and (pair? olddivisors) (car olddivisors))]
		  [quotient (and (pair? oldquotients) (car oldquotients))])
	      (cond [divisionoccured
		     (loop1
		      dividend
		      divisors
		      (append (reverse newquotients) oldquotients)
		      remainder)]
		    [(null? oldquotients)
		     (let ([p (term->polynomial (lt dividend))])
		       (loop1
			(p- dividend p)
			divisors
			(append (reverse newquotients) oldquotients)
			(p+ remainder p)))]
		    [(divides (lt divisor) (lt dividend))
		     (let* ([t (t/ (lt dividend) (lt divisor))]
			    [p (term->polynomial t)])
		       (loop2
			newquotients
			(cons (p+ quotient p) (cdr oldquotients))
			(p- dividend (p* p divisor))
			newdivisors
			olddivisors
			#t))]
		    [else
		     (loop2
		      (cons quotient newquotients)
		      (cdr oldquotients)
		      dividend
		      (cons divisor newdivisors)
		      (cdr olddivisors)
		      divisionoccured)]))))))

  (define (quotients f . g)
    (let-values ([(q r) (apply divide f g)]) q))

  (define (remainder f . g)
    (let-values ([(q r) (apply divide f g)]) r))
  )

