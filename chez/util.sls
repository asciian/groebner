(library (util)
  (export repeat unique map-combination)
  (import (rnrs base))
  
  (define (repeat e n)
    (if (zero? n) '() (cons e (repeat e (- n 1)))))
  
  (define (unique r)
    (if (null? r) '() (cons (car r) (unique (cdr r)))))
  
  (define (map-combination proc . lists)
    (define (rec c)
      (define (rec-1 . d) (apply proc c d))
      (apply map-combination rec-1 (cdr lists)))
    (if (null? (cdr lists))
	(map proc (car lists))
	(apply append (map rec (car lists)))))
  )
