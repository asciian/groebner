#!/usr/bin/env python3
from sympy import *
import time
x, y, z = symbols('x,y,z')

init_printing(use_unicode=False, wrap_line=False, no_global=True)

start = time.time()
for i in range(1000):
    groebner([x*z-y**2, x**3-z**2], x, y, z, order='lex')
elapsed_time = time.time() - start

print ("elapsed_time:{0}".format(elapsed_time) + "[sec]")
