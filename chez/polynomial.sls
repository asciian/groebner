#!r6rs
(library (polynomial)
  (export p= p+ p- p* p^ term->polynomial monomial->polynomial)
  (import (rnrs base)
	  (rnrs lists)
	  (rnrs sorting)
	  (util)
	  (compose)
	  (monomial)
	  (ordering)
	  (term))
  
  (define (acons k v a)
    (cons (cons k v) a))

  (define (p= p q)
    (define (comp s t)
      ((ordering) (car s) (car t)))
    (equal? (list-sort comp p) (list-sort comp q)))
  
  (define (p+ p q)
    (if (or (null? p) (null? q))
	(remp (compose zero? cdr) (append p q))
	(let* ([q1 (car q)]
	       [q2 (cdr q)]
	       [q? (lambda (r) (m= (car r) (car q1)))]
	       [p1 (find q? p)]
	       [p2 (remp q? p)])
	  (if p1
	      (p+ (acons (car p1) (+ (cdr p1) (cdr q1))  p2) q2)
	      (p+ (cons q1 p2) q2)))))
  
  (define (p- p q)
    (if (or (null? p) (null? q))
	(remp (compose zero? cdr) (append p q))
	(let* ([q1 (car q)]
	       [q2 (cdr q)]
	       [q? (lambda (r) (m= (car r) (car q1)))]
	       [p1 (find q? p)]
	       [p2 (remp q? p)])
	  (if p1
	      (p- (acons (car p1) (- (cdr p1) (cdr q1)) p2) q2)
	      (p- (acons (car q1) (- (cdr q1)) p2) q2)))))
  
  (define (p* p q)
    (let ploop ([p1 (car p)] [p2 (cdr p)] [r1 '()])
      (let qloop ([q1 (car q)] [q2 (cdr q)] [r2 r1])
	(cond [(pair? q2) (qloop (car q2) (cdr q2) (p+ r2 (list (t* p1 q1))))]
	      [(pair? p2) (ploop (car p2) (cdr p2) (p+ r2 (list (t* p1 q1))))]
	      [else (p+ r2 (list (t* p1 q1)))]))))  
  
  (define (p^ p n)
    (define (mul . p)
      (fold-left t* (car p) (cdr p)))
    (define (add . t)
      (fold-left p+ (list (car t)) (map list (cdr t))))
    (apply add (apply map-combination mul (repeat p n))))
  
  (define (term->polynomial t)
    (list t))
  
  (define (monomial->polynomial m)
    (term->polynomial (monomial->term m)))
  )
