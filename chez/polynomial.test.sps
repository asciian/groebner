#!/usr/bin/env chez --script

(import (polynomial)
	(parser))

(define p parse)
(assert (p= (p* (p "x+y") (p "x+y")) (p "x^2+2*x*y+y^2")))
(assert (p= (p^ (p "x+y") 2) (p "x^2+2*x*y+y^2")))
